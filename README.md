# php-extended/php-tld-provider-mozilla

An implementation of the php-tld-interface library.

![coverage](https://gitlab.com/php-extended/php-tld-provider-mozilla/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-tld-provider-mozilla/badges/master/coverage.svg?style=flat-square)


## Last Updated Date : 2025-01-26


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-tld-provider-mozilla ^8`


## Basic Usage

You may use this library this way to build a hierarchy :

```php

use PhpExtended\Tld\MozillaTopLevelDomainProvider;

$provider = new MozillaTopLevelDomainProvider();
$hierarchy = $provider->getHierarchy();
$hierarchy->isTld('example.com'); // returns false

```


## License

MIT (See [license file](LICENSE)).