<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-tld-provider-mozilla library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Tld\MozillaTopLevelDomainProvider;
use PHPUnit\Framework\TestCase;

/**
 * MozillaTopLevelDomainProviderTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Tld\MozillaTopLevelDomainProvider
 *
 * @internal
 *
 * @small
 */
class MozillaTopLevelDomainProviderTest extends TestCase
{
	
	/**
	 * The provider to test.
	 * 
	 * @var MozillaTopLevelDomainProvider
	 */
	protected MozillaTopLevelDomainProvider $_provider;
	
	public function testToString() : void
	{
		$object = $this->_provider;
		$this->assertEquals(\get_class($object).'@'.\spl_object_hash($object), $object->__toString());
	}
	
	public function testHierarchyContainsLv1() : void
	{
		$hierarchy = $this->_provider->getHierarchy();
		$this->assertTrue($hierarchy->isTld('com'));
	}
	
	public function testHierarchyContainsLv2() : void
	{
		$hierarchy = $this->_provider->getHierarchy();
		$this->assertTrue($hierarchy->isTld('co.uk'));
	}
	
	public function testHierarchyUtf8() : void
	{
		$hierarchy = $this->_provider->getHierarchy();
		$this->assertTrue($hierarchy->isTld('网络.cn'));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_provider = new MozillaTopLevelDomainProvider();
	}
	
}
