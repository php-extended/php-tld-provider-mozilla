<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-tld-provider-mozilla library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Tld;

use RuntimeException;

/**
 * MozillaTopLevelDomainProvider class file.
 * 
 * This provides the top level domain hierarchy from the embedded top level
 * domain list.
 * 
 * @author Anastaszor
 */
class MozillaTopLevelDomainProvider implements TopLevelDomainProviderInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Tld\TopLevelDomainProviderInterface::getHierarchy()
	 */
	public function getHierarchy() : TopLevelDomainHierarchyInterface
	{
		$path = \dirname(__DIR__).\DIRECTORY_SEPARATOR.'dat'.\DIRECTORY_SEPARATOR.'public_suffix_list.dat';
		$contents = \file_get_contents($path);
		if(false === $contents)
		{
			// @codeCoverageIgnoreStart
			$message = 'Failed to read the public suffix list at {path}';
			$context = ['{path}' => $path];
			
			throw new RuntimeException(\strtr($message, $context));
			// @codeCoverageIgnoreEnd
		}
		
		$root = new TopLevelDomainNode('root');
		
		foreach(\array_map('trim', \explode("\n", $contents)) as $fileLine)
		{
			// avoid empty lines
			if(0 === \mb_strlen($fileLine))
			{
				continue;
			}
			
			// avoid comment lines
			if(0 === \mb_strpos($fileLine, '//'))
			{
				continue;
			}
			
			$domain = \explode('.', $fileLine);
			/** @var TopLevelDomainNode $node */
			$node = $root;
			
			while(!empty($domain))
			{
				$part = (string) \array_pop($domain); // from the end
				
				if(0 === \mb_strlen($part))
				{
					continue;
				}
				
				if('*' === $part[0])
				{
					$node->setChildrenAreTlds(true);
					continue;
				}
				
				if('!' === $part[0])
				{
					$part = (string) \mb_substr($part, 1);
					$newNode = new TopLevelDomainNode($part);
					$node->addNonTldNode($newNode);
					$node = $newNode;
					continue;
				}
				
				$newNode = new TopLevelDomainNode($part);
				$node->addTldNode($newNode);
				$node = $newNode;
			}
		}
		
		return new TopLevelDomainHierarchy(\array_values(\iterator_to_array($root->getTldChildrenNodes())));
	}
	
}
